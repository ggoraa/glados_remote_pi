#!/bin/sh

# startup system information text
echo GLaDOS system v3.11
echo by Aperture Laboratories
sleep 0.5
echo RAM:
cat /proc/meminfo
echo ROM:
cat /proc/partitions
df -h
echo Installed packages:
dpkg --get-selections
echo Available disk space:
free
echo IP of GLaDOS system:
hostname -I
echo Connected USB devices:
lusb
echo CPU Temperature:
vcgencmd measure_temp
echo Nearby Wi-Fi hotspots:
iwlist wlan0 scan

cd /home/pi/system # goes to scripts folder
./launcher.sh # starts launcher
