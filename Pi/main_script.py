import pyaudio
import wave
import os
import RPi.GPIO as GPIO
import time
from time import sleep

# pin definitions
eyeLEDPin = 18
bodyServoPin = 19
frontServoPin = 15
turnServoPin = 16
headServoPin = 20
powerButtonPin = 21

# setup GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(eyeLEDPin, GPIO.OUT)
GPIO.setup(frontServoPin, GPIO.OUT)
GPIO.setup(bodyServoPin, GPIO.OUT)
GPIO.setup(headServoPin, GPIO.OUT)
GPIO.setup(turnServoPin, GPIO.OUT)
GPIO.setup(powerButtonPin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
bodyServoPWM = GPIO.PWM(bodyServoPin, 100)
frontServoPWM = GPIO.PWM(frontServoPin, 100)
headServoPWM = GPIO.PWM(headServoPin, 100)
turnServoPWM = GPIO.PWM(turnServoPin, 100)

GPIO.output(eyeLEDPin, GPIO.LOW)

def powerbutton_callback(channel) : # power button code
    print("Power button pushed")
    subprocess.call(./shutdown.sh)
    
def eye_startup_blink() :
    GPIO.output(
