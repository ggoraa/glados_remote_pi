#!/bin/sh

# update the packages
sudo apt update
sudo apt upgrade

mkdir /home/pi/system # create a scripts folder, if doesn't exist
cd /home/pi/system # goes to scripts directory

# updates software of GLaDOS
rm servo_control.py # deletes old script
git clone https://ggoraa@bitbucket.org/ggoraa/glados-scripts.git # downloads new version
cd /home/glados-scripts # goes to repo folder
# moves new files to script folder
mv servo_control.py /home/pi/system
mv launcher.py /home/pi/system
cd /home
rm /home/glados_scripts #deletes repo folder
