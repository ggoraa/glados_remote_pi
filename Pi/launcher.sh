#!/bin/sh

echo Updating system...
./updater.sh #starts updates

echo Setting up audio...
amixer cset numid=3 1

echo Connecting to network...
iwconfig wlan0 essid NETWORK_NAME key WIRELESS_KEY

echo Starting self test...
# code for checking internet connection, servo check, microphone and speakers

echo Launching...
chmod +x main_script.py # giving execute permissions to python file
./main_script.py # executes program
