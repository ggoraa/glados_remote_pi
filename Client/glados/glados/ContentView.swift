//
//  ContentView.swift
//  glados
//
//  Created by Егор Яковенко on 15.02.2020.
//  Copyright © 2020 GGorAA. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
            .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
